FROM rust as builder
WORKDIR /app
COPY . .
RUN cargo install --path .

FROM debian:bookworm-slim
RUN apt update && apt install -y sqlite3

COPY --from=builder /usr/local/cargo/bin/status /app/status
COPY --from=builder /app/frontend /frontend

CMD ["/app/status"]
EXPOSE 8080
