const $ = document.querySelector.bind(document);
const $$ = document.querySelectorAll.bind(document);

const byteFields = ["memory/total_memory", "memory/used_memory", "memory/cached_memory", "memory/total_swap", "memory/used_swap", "network/total_received", "network/total_transmitted", "network/received", "network/transmitted", "disk/total_storage", "disk/used_storage", "disk/available_storage"];
const percentageFields = ["cpu/usage"];
const hertzFields = ["cpu/frequency"];
const dateFields = ["host/boot_time"];
const timeFields = ["host/uptime"];

$("#title").onclick = function() {
  window.history.back();
  fetchApi();
};

window.addEventListener("hashchange", function() {
  fetchApi();
});

async function fetchApi() {
  const response = await fetch("/api/status");
  const json = await response.json();

  if (window.location.hash) {
    showDetails(json, window.location.hash.replace("#", ""));
  } else {
    showSummary(json);
  }
}

function showSummary(response) {
  $("#title").innerText = "Status";
  $("#content").innerHTML = "";

  appendElementToSummary(response, "host", formatData(response, "host/operating_system"));
  appendElementToSummary(response, "cpu", formatData(response, "cpu/usage"));
  appendElementToSummary(response, "memory", formatData(response, "memory/used_memory") + " / " + formatData(response, "memory/total_memory"));
  appendElementToSummary(response, "network", formatData(response, "network/received") + " received, " + formatData(response, "network/transmitted") + " transmitted");
  appendElementToSummary(response, "disk", formatData(response, "disk/used_storage") + " / " + formatData(response, "disk/total_storage"));
}

function appendElementToSummary(response, field, value) {
  let element = createInfoElement(field.toTitleCase(), value);

  element.onclick = function(_) {
    window.location.hash = field;
    showDetails(response, field);
  }

  $("#content").appendChild(element);
}

function showDetails(response, field) {
  $("#title").innerText = field.toTitleCase();
  $("#content").innerHTML = "";

  for (let key of Object.keys(response[field])) {
    const el = createInfoElement(key.replace("_", " ").toTitleCase(), formatData(response, `${field}/${key}`));
    $("#content").appendChild(el);
  }
}

function createInfoElement(title, value) {
  const divEl = document.createElement("div");
  const titleEl = document.createElement("h3");
  const contentEl = document.createElement("p");

  titleEl.innerText = title;
  contentEl.innerText = value;

  divEl.appendChild(titleEl);
  divEl.appendChild(contentEl);
  
  return divEl;
}

function formatData(response, key) {
  const fieldSplit = key.split("/");
  const value = response[fieldSplit[0]][fieldSplit[1]];

  if (percentageFields.includes(key)) {
    return `${Number(value).round()}%`;
  }

  if (byteFields.includes(key)) {
    let num = Number(value);
    let index = 0;
    while (num > 1024) {
      index++;
      num /= 1024;
    }
    return `${num.round()}${" kMGT"[index]}B`.replace(" ", "")
  }

  if (hertzFields.includes(key)) {
    return `${(Number(value) / 1000).round()}GHz`
  }

  if (dateFields.includes(key)) {
    return new Date(Number(value) * 1000).toLocaleString();
  }

  if (timeFields.includes(key)) {
    let diff = Number(value);

    let ss = Math.floor(diff) % 60;
    let mm = Math.floor(diff / 60) % 60;
    let hh = Math.floor(diff / 60 / 60);

    return `${hh}hr, ${mm}min, ${ss}sec`;
  }

  return value;
}

String.prototype.toTitleCase = function() {
  return this.replace(
    /\w\S*/g,
    function(txt) {
      return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
    }
  );
}

Number.prototype.round = function() {
  return Math.round(this * 100) / 100;
}

fetchApi();

setInterval(fetchApi, 1000);
