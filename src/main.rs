#![warn(clippy::all, clippy::pedantic, clippy::nursery)]
#![allow(clippy::cast_possible_truncation)]
#![allow(clippy::cast_sign_loss)]
#![allow(clippy::cast_precision_loss)]

use actix_files::{Files, NamedFile};
use actix_web::{get, web, App, HttpServer, Responder, Result};
use libmacchina::{GeneralReadout, MemoryReadout, NetworkReadout};
use once_cell::sync::Lazy;
use serde::Serialize;
use std::env::consts::ARCH;
use std::sync::Mutex;
use std::time::{SystemTime, UNIX_EPOCH};

const DEFAULT_REFRESH_INTERVAL_MS: u64 = 1000;
static REFRESH_INTERVAL_MS: Lazy<u64> = Lazy::new(|| {
    std::env::var("REFRESH_INTERVAL_MS").map_or(DEFAULT_REFRESH_INTERVAL_MS, |interval| {
        interval
            .parse::<u64>()
            .unwrap_or(DEFAULT_REFRESH_INTERVAL_MS)
    })
});
static NETWORK_INTERFACE: Lazy<Option<String>> =
    Lazy::new(|| std::env::var("NETWORK_INTERFACE_NAME").ok());

static DATA: Lazy<Mutex<SystemData>> = Lazy::new(|| Mutex::new(SystemData::default()));

// remember total rx and tx to calculate diff from the last few secs
static TOTAL_TX: Lazy<Mutex<usize>> = Lazy::new(|| Mutex::new(0));
static TOTAL_RX: Lazy<Mutex<usize>> = Lazy::new(|| Mutex::new(0));

#[derive(Clone, Serialize, Default)]
struct OsInfo {
    operating_system: Option<String>,
    hostname: Option<String>,
    uptime: usize,
    boot_time: usize,
}

#[derive(Clone, Serialize, Default)]
struct CpuData {
    name: Option<String>,
    architecture: Option<String>,
    usage: Option<usize>,
    cores: usize,
}

#[derive(Clone, Serialize, Default)]
struct MemoryData {
    total_memory: u64,
    used_memory: u64,
    cached_memory: u64,
    total_swap: u64,
    used_swap: u64,
}

#[allow(clippy::struct_field_names)]
#[derive(Clone, Serialize, Default)]
struct DiskData {
    total_storage: u64,
    used_storage: u64,
    available_storage: u64,
}

#[derive(Clone, Serialize, Default)]
struct NetworkData {
    interface_name: String,
    total_received: usize,
    total_transmitted: usize,
    received: usize,
    transmitted: usize,
}

#[derive(Clone, Serialize, Default)]
struct SystemData {
    host: OsInfo,
    cpu: CpuData,
    memory: MemoryData,
    network: Option<NetworkData>,
    disk: Option<DiskData>,
}

#[get("/")]
async fn index() -> Result<NamedFile> {
    Ok(NamedFile::open("./frontend/index.html")?)
}

#[get("/api/status")]
async fn api() -> Result<impl Responder> {
    let system_data = DATA.lock().unwrap().clone();
    Ok(web::Json(system_data))
}

fn extract_data() -> SystemData {
    use libmacchina::traits::GeneralReadout as _;
    use libmacchina::traits::MemoryReadout as _;
    use libmacchina::traits::NetworkReadout as _;

    let general_readout = GeneralReadout::new();
    let mem_readout = MemoryReadout::new();
    let network_readout = NetworkReadout::new();

    let host = OsInfo {
        operating_system: general_readout.distribution().ok(),
        hostname: general_readout.hostname().ok(),
        uptime: general_readout.uptime().unwrap_or_default(),
        boot_time: general_readout
            .uptime()
            .map(|sec| {
                SystemTime::now()
                    .duration_since(UNIX_EPOCH)
                    .unwrap()
                    .as_millis()
                    / 1000
                    - (sec as u128)
            })
            .unwrap_or_default() as usize,
    };

    let cpu = CpuData {
        name: general_readout.cpu_model_name().ok(),
        architecture: Some(ARCH.to_owned()),
        usage: general_readout.cpu_usage().ok(),
        cores: general_readout.cpu_cores().unwrap_or_default(),
    };

    let memory = MemoryData {
        total_memory: mem_readout.total().unwrap_or_default() * 1024,
        used_memory: mem_readout.used().unwrap_or_default() * 1024,
        cached_memory: mem_readout.cached().unwrap_or_default() * 1024,
        total_swap: mem_readout.swap_total().unwrap_or_default() * 1024,
        used_swap: mem_readout.swap_used().unwrap_or_default() * 1024,
    };

    let network = NETWORK_INTERFACE.clone().map(|if_name| {
        let tx = network_readout.tx_bytes(Some(if_name.as_str()));
        let rx = network_readout.rx_bytes(Some(if_name.as_str()));

        if let (Ok(rx), Ok(tx)) = (rx, tx) {
            let mut rx_lock = TOTAL_RX.lock().unwrap();
            let mut tx_lock = TOTAL_TX.lock().unwrap();

            let rx_diff = rx - *rx_lock;
            let tx_diff = tx - *tx_lock;

            *rx_lock = rx;
            *tx_lock = tx;
            drop(rx_lock);
            drop(tx_lock);

            let mul_factor = 1000.0 / (DEFAULT_REFRESH_INTERVAL_MS as f64);

            NetworkData {
                interface_name: if_name,
                total_received: rx,
                total_transmitted: tx,
                received: (rx_diff as f64 * mul_factor) as usize,
                transmitted: (tx_diff as f64 * mul_factor) as usize,
            }
        } else {
            NetworkData::default()
        }
    });

    let disk = general_readout
        .disk_space()
        .map(|disk| DiskData {
            total_storage: disk.1,
            used_storage: disk.0,
            available_storage: disk.1 - disk.0,
        })
        .ok();

    SystemData {
        host,
        cpu,
        memory,
        network,
        disk,
    }
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    actix_web::rt::spawn(async {
        loop {
            *DATA.lock().unwrap() = extract_data();
            actix_rt::time::sleep(std::time::Duration::from_millis(*REFRESH_INTERVAL_MS)).await;
        }
    });

    HttpServer::new(move || {
        App::new()
            .service(api)
            .service(index)
            .service(Files::new("/", "./frontend"))
    })
    .bind(("0.0.0.0", 8080))?
    .run()
    .await
}
